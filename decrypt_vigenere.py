from binascii import hexlify, unhexlify
import string, sys

known_freq = [0.0 for i in range(256)]
known_freq[ord('a')] = 0.08167	
known_freq[ord('b')] = 0.01492	
known_freq[ord('c')] = 0.02782	
known_freq[ord('d')] = 0.04253	
known_freq[ord('e')] = 0.12702	
known_freq[ord('f')] = 0.02228	
known_freq[ord('g')] = 0.02015	
known_freq[ord('h')] = 0.06094	
known_freq[ord('i')] = 0.06966	
known_freq[ord('j')] = 0.00153	
known_freq[ord('k')] = 0.00772	
known_freq[ord('l')] = 0.04025	
known_freq[ord('m')] = 0.02406	
known_freq[ord('n')] = 0.06749	
known_freq[ord('o')] = 0.07507	
known_freq[ord('p')] = 0.01929	
known_freq[ord('q')] = 0.00095	
known_freq[ord('r')] = 0.05987	
known_freq[ord('s')] = 0.06327	
known_freq[ord('t')] = 0.09056	
known_freq[ord('u')] = 0.02758	
known_freq[ord('v')] = 0.00978	
known_freq[ord('w')] = 0.02360	
known_freq[ord('x')] = 0.00150	
known_freq[ord('y')] = 0.01974	
known_freq[ord('z')] = 0.00074	


def populate_pt(pt, pos, key_len):
	global final_pt
	j = 0
	for i in range(pos, len(final_pt), key_len):
		final_pt[i] = pt[j]
		j += 1

def check_freq_dist(pt_stream):
	global known_freq
	f_dist = [0.0 for i in range(256)]
	for i in range(len(pt_stream)):
		f_dist[ord(pt_stream[i])] += 1
	acc = 0.0
	for i in range(len(f_dist)):
		acc += f_dist[i]
	
	for i in range(len(f_dist)):
		f_dist[i] = f_dist[i]/ float(acc)
	
	#got the freq dist
	acc = 0.0
	for i in range(len(f_dist)):
		acc += known_freq[i] * f_dist[i]
	print "Acc is %f" %(acc)
	return acc

def check_pt_stream(cpt_stream):

	allowed_chars = string.ascii_uppercase + string.ascii_lowercase + string.digits   + ".,- " # need to add space as well
	for i in range(len(cpt_stream)):
		if cpt_stream[i] not in allowed_chars:
			return 0.0
		
	print "%s in allowed chars  and len is %d" % (cpt_stream, len(cpt_stream))
	print "checking freq dist"
	acc = check_freq_dist(cpt_stream)
	return acc

def decrypt(key_len, ct):
	"""
So how do we decrypt it, now that we know the len of the key? lets say the key len is 'n'. Taking every
n'th character of the CT will ensure that all these CT bytes have been encrypted with the same key byte.
Brute force the key byte for all possible possibilities from 0-255. 
What happens when the key byte that we are using is the _actual_ key byte. We can observe 2 things in that case,
1) All the resulting PT bytes are between >32 & <= 127 (ASCII printable range)
2) calculate the freq dist of the lower case characters among the PT bytes. let it be Qi, and the known freq
dist of lower case english letters be Pi, then summation(Qi * Pi) ~ summation(Pi * Pi) ~ 0.065
	"""
	global known_freq, final_pt
	cipher_text = ct
	ct = unhexlify(cipher_text)
	for i in range(key_len): #iterate thro' every key_len + i bytes
		ct_stream = ""
		for j in range(i, len(ct), key_len):
			ct_stream += ct[j]
		#got ct stream, now bruteforce
		canditate_pt_stream = ""
		candidate_dist = 0.0
		for k in range(256):
			pt_stream = ""
			for l in range(len(ct_stream)):
				pt_stream += chr(ord(ct_stream[l]) ^ k)
			ret_dist = check_pt_stream(pt_stream)
			if ret_dist != 0.0: 
				if ret_dist > candidate_dist:
					candidate_dist = ret_dist
					canditate_pt_stream = pt_stream
		print "final canditate_pt_stream is " + canditate_pt_stream
		populate_pt(canditate_pt_stream, i, key_len)	
	print "############### PLAINTEXT ##################"
	print "".join(final_pt)

def get_max(freq_list):
	maxi = 0.0
	key_len = 0
	for i in range(len(freq_list)):
		if freq_list[i][0] > maxi :
			maxi = freq_list[i][0]
			key_len = freq_list[i][1]
	
	print "key len is %d and freq is %f" % (key_len, maxi)
	return key_len

def calculate_dist(freq):
	acc = 0.0
	for i in range(len(freq)):
		acc += freq[i]
	for i in range(len(freq)):
		freq[i] = freq[i] / float(acc)
	acc = 0.0
	for i in range(len(freq)):
		acc += freq[i] ** 2
	return acc

def get_key_len(ct):
	"""
How can we know the key len? Let's say the key len is 4, which would mean, that every 4th character
of the plaintext is shifter/xor'ed with the same key byte. For eg) we know that the most frequently 
occuring alphabet in english is 'e', by choosing only the 4th byte of the ciphertext and computing
frequencies of those, the most occuring byte of the CT corresponds to the PT byte 'e', but this would 
result us in computing freq dist of all printable chars. There is a better way to do this. The frequency 
distribution of english alphabets is far from uniform. Taking the '4'th byte would result in a distribution
which is just a permuted version of the original distribution of the english letters + ascii printable chars.
But if the length of the key is wrong, we get a distribution closer to uniform, lets say the wrong key len is 3, 
then all the 3rd characters of the PT are __not__ xor'd/shifted with the same CT byte. To be more accurate
compute the sum of all Pi ^2, and choose the greatest of them , which would correspond to the correct key len
	"""
	cipher_text = ct
	freq_list = []
	ct = unhexlify(cipher_text)
	for key_len in range(1, 20):
		freq = [0.0 for i in range(256)]
		for i in range(0, len(ct), key_len):
			freq[ord(ct[i])] += 1
		dist = calculate_dist(freq)
		freq_list.append((dist, key_len))

	klen = get_max(freq_list)
	return klen

def vigenere_decrypt(ct):
	# first need to guess the key len, for that we need the frequency distribution of 
	# alphabets in the english language
	key_len = get_key_len(ct)
	print "key_len is %d" %(key_len)
	decrypt(key_len, ct)


if __name__ == "__main__":
	global final_pt
	if len(sys.argv) != 2:
		print "Usage: %s <ct file>"
		print "Note that CT should be in hex format"
		sys.exit(0)
	f = open(sys.argv[1], "r")
	ct = f.readline()
	ct = ct.strip()
	final_pt = ['' for i in range(len(ct) / 2)]
	vigenere_decrypt(ct)
